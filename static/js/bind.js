var data=[];

//function addBr(text){
//    return text.replace(/\n/g, "<br />");
//
//}
var Message;
Message = function (arg) {
    this.text = arg.text, this.message_side = arg.message_side;
    this.draw = function (_this) {
        return function () {
            var $message;
            $message = $($('.message_template').clone().html());
//            $message.addClass(_this.message_side).find('.text').html(addBr(_this.text));
            $message.addClass(_this.message_side).find('.text').html(_this.text);
            $('.messages').append($message);
            return setTimeout(function () {
                return $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
};


function showBotMessage(msg){
        message = new Message({
             text: msg,
             message_side: 'left'
        });
        message.draw();
        $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
}
function showUserMessage(msg){
        $messages = $('.messages');
        message = new Message({
            text: msg,
            message_side: 'right'
        });
        message.draw();
        $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        $('#msg_input').val('');
//        $('.checkbox_input').val('');
}

//function get_text(text1) {
////    text_lst = []
//    $('.text1').on("click", function() {
//        text1 = $(this).text();
////        text_lst = text_lst.push(text1)
////        console.log("=====", text_lst)
//        showUserMessage(text1)
//        sayToBot(text1)
//    });
//}

//$(document).on('click', ".text1", function () {
//    // point the obj variable to the clicked element
//    text1 = $(this).text();
//    console.log(text1)
//    if(text1 == "Incident Summary Open Detail") {
//         $('.dia-text').text("Section I: Incident Summary \n" +
//                "\n" +
//                "Incident Reference Number \n" +
//                "As in Incident Log \n" +
//                "RCA Technology Owner \n" +
//                "(Support Group Name / Name of the person) \n" +
//                "\n" +
//                "Date of the Incident \n" +
//                "As in Incident Log \n" +
//                "RCA Process owner \n" +
//                "(Service Improvement / Name of the person) \n" +
//                "\n"+
//                "Time of the Incident \n" +
//                "As in Incident Log \n" +
//                "RCA Technology approval \n" +
//                "(Name of the Tech. Manager who approves) \n" +
//                "\n"+
//                "Duration of outage \n" +
//                "As in Incident Log \n" +
//                "RCA initiated on \n" +
//                "\n"+
//                "(DD/MM/YYYY) \n" +
//                "\n"+
//                "Impacted Services \n" +
//                " \n" +
//                "RCA submitted on \n" +
//                "(DD/MM/YYYY) \n" +
//                "\n"+
//                "Short Description \n" +
//                "\n"+
//                "\n"+
//
//
//                "Management Summary: (Non-Technical; Not more than 4 lines) \n" +
//
//                "What went wrong and when, causing impact to whom? How was it resolved? What is the root cause?  \n" +
//                "Chronology Of Events: \n" +
//                "\n"+
//                "What happened from the time the incident was reported till the time the incident got resolved. \n" +
//                 "Snap shot of actions along with the time of the action \n" +
//
//
//                "Background:  \n" +
//                "When the incident happened, what were the situations / operations that the systems / network / infrastructure /users were  \n" +"involved in? \n" +
//                "\n"+
//                "\n"+
//                "Symptoms: \n" +
//                "\n"+
//                "What was the symptom of the issue, if any?  \n" +
//                "Were any symptoms there prior to the incident? \n"
//                );
//    }
//    else if(text1 == "GROUPED INCIDENTS SUMMARY Open Detail") {
//         $('.dia-text').text("Incident Reference Number(s) \n"+
//            "As in Incident Log \n"+
//            "RCA Technology Owner \n"+
//            "(Support Group Name / Name of the person) \n"+
//            "Category of Incidents \n"+
//            "As in Incident Log \n"+
//            "RCA Process owner \n"+
//            "(Service Improvement / Name of the person) \n"+
//            "RCA initiated on \n"+
//            "(DD/MM/YYYY) \n"+
//            "RCA Technology approval \n"+
//            "(Name of the Tech. Manager who approves) \n"+
//            "RCA submitted on \n"+
//            "(DD/MM/YYYY) \n"+
//            "\n"+
//            "Short Description \n"+
//            "Description of the grouping or selected incidents under a common category for a common root cause \n"+
//            "\n"+
//            "Management Summary: (Non-Technical; Not more than 4 lines) \n"+
//            "What went wrong and when, causing impact to whom? How was they resolved? What may be the underlying root cause?  \n"+
//            "\n"+
//            "Background: \n"+
//            "When the incident happened, what were the situations / operations that the systems / network / infrastructure /users were \n"+
//            "involved in? \n"+
//            "\n"+
//            "\n"+
//            "Symptoms: \n"+
//            "What was the symptom of the issue, if any?  \n"+
//            "Were any symptoms there prior to the incident? \n"
//            );
//    }
//    else if(text1 == "RESOLUTION Open Detail") {
//         $('.dia-text').text("Root Cause Identification:\n"+
//                "\n"+
//                "How was/were the incident(s) resolved?\n"+
//                "Who resolved the incident(s)?\n"+
//                "Was it a workaround or permanent solution?\n"+
//                "Was the resolution confirmed with the user / user community?\n"+
//                "\n"+
//                "Iteration 1:\n"+
//                "\n"+
//                "What are the cause/s and root cause?\n"+
//                "Use of fish bone structure as applicable\n"+
//                "\n"+
//                "\n"+
//                "\n"+
//                "Iteration 2:\n"+
//                "\n"+
//                "When analysing cause 1 – what was observed / identified? \n"+
//                "Use of fish bone structure as applicable\n"+
//                "\n"+
//                "Iteration 3:\n"+
//                "\n"+
//                "When analysing cause 2 – what was observed / identified?\n"+
//                "Use of fish bone structure as applicable\n"+
//                "\n"+
//                "Root cause :\n"+
//                "\n"+
//                "When analysing cause 3 – what was observed / identified?\n"
//                );
//    }
//    else if(text1 == "ACTIONS TO PREVENT FURTHER OCCURENCE Open Detail") {
//         $('.dia-text').text("SECTION IV: ACTIONS TO PREVENT FURTHER OCCURENCE\n"+
//                    "\n"+
//                    "Any specific corrective or preventive actions identified to\n"+
//                    "ensure the incidents do not not reoccur / or impact is reduced in next occurrence.\n"
//                    );
//    }
//
//
//     else if(text1 == "Introduction Open Detail") {
//         $('.dia-text').text("Service continuity plan, in the event of a disruption, enables the organization to resume \n" +
//             "service delivery.  \n" +
//            " \n" +
//            "Creating the service continuity plan typically involves the following three activities.\n"  +
//            "Creating the continuity plan based on the information previously collected \n" +
//            "Creating test conditions / scenarios to  the tests to validate the execution of plan \n" +
//            "Creating training materials / training delivery methods for informing the stakeholders \n" +
//             "to take ownership of the service continuity plan \n" +
//            "1.1 Scope \n" +
//            "This process aims to provide proactive guidance for all the units as defined in Quality \n" +
//             "Manual for service continuity planning programs to ensure smooth service delivery for \n" +
//              "the critical services following a disaster or a calamity.   \n" +
//            "1.2 List of Procedures \n" +
//            "1. Plan for service continuity \n" +
//            "2. Develop Service Continuity Plans  & Training \n" +
//            "3. Prepare for the Verification and Validation  \n" +
//            "4. Verify and Validate the Service Continuity Plan \n" +
//            "5. Analyze Results  \n" +
//            " \n" +
//            "1.3 Abbreviations \n"	 +
//            "QA / PPQA \n" +
//            "Software Quality Advisor /  Process and Product Quality Both the terms may \n" +
//             "be used interchangeably \n" +
//            "SCON / BCM \n" +
//            "Service Continuity / Business Continuity Management \n" +
//            "QM \n" +
//            "Quality Manager – the person / team in-charge of the process improvement \n" +
//             "initiative in the organization \n" +
//            "PMP \n" +
//            "Project Management Plan \n" +
//            "QM \n" +
//            "Quality Manager – the person / team in-charge of the process improvement  \n" +
//            "initiative in the organization. \n"
//            );
//    }
//     else if(text1 == "Service Continuity ProcessOpen Detail") {
//         $('.dia-text').text("Organization needs / Inputs from Management for establishing service continuity \n"+
//                "Services established (service catalog) \n"+
//                "Customer explicit requirements for Service Continuity \n"+
//                "Entry Criteria \n"+
//                "Services identified (service Catalogue) \n"+
//                "Identified & prioritized essential functions and resources \n"+
//                " \n"+
//                "Exit Criteria \n"+
//                "Proactive planning and verification mechanisms are in place to ensure \n"+
//                 "that the organization is capable to ensure business continuity \n"+
//                " \n"+
//                "Activity / Task Description \n"+
//                "Responsibility \n"+
//                "Reference Assets \n"+
//                " \n"+
//                "Primary \n"+
//                "Secondary \n"+
//                " \n"+
//                "Critical services to the department and the critical functions / \n"+
//                 "sub systems/resources which are required to provide each service are \n"+
//                 "identified. (Refer to Capacity and availability Management – CAM strategy \n"+
//                  "and the service catalogue) \n"+
//                "Service team / PM \n"+
//                "Stakeholders \n"+
//                "CAM Strategy in PMP \n"+
//                "CAM Plan \n"+
//                "For the identified critical sub-systems / sub services/ resources / activities, \n"+
//                "prioritize and analyze the possible risks associated with the failure of the same. Log the same as risks \n"+
//                "Service team / PM \n"+
//                "Stakeholders \n"+
//                " \n"+
//                "PMP annexure \n"
//                );
//    }
//     else if(text1 == "Support ProcessesOpen Detail") {
//         $('.dia-text').text("Support Processes \n"+
//                "3.1 Measurements \n"+
//                "Effort spent on various Service Continuity activities. \n"+
//                "Planned versus Actual SCP Test conducted \n"+
//                "Planned versus Actual Backup and Restoration done \n"+
//                "3.2 Verification and Validation \n"+
//                "The QA audits and internal audits  \n"+
//                "PM concerned on demand \n"+
//                "Concerned QA by surprise verification \n"+
//                "Periodical audits by external team (randomly selected)  \n"+
//                "At least once during project duration (for projects with duration < 3 months duration)  \n"+
//                "Once every three months for other projects (randomly selected). \n"+
//                " \n"+
//                "Senior Management periodically reviews the overall Service Continuity activities performed. \n"+
//                "What If scenarios and the preparedness by the team will be done to proactively check instances like  \n"+
//                "Processing systems NOT available \n"+
//                "Data / Code got corrupted \n"+
//                "Not able to access the above even when they are available due to data links failure \n"+
//                " \n"+
//                "3.3 Tailoring Guidelines \n"+
//                "The recommended solution can be piloted based on the discretion of the stakeholders before the final approval. \n"
//                );
//    }
//
//
//    else if(text1 == "PurposeOpen Detail") {
//        $('.dia-text').text("Purpose \n"+
//                "The purpose of Service Strategy Process is to identify the capabilities \n"+
//                "of the organization and to maintain the standard set of services and \n"+
//                "description in line with the capabilities. Standard set of services \n"+
//                "will help in showcasing the organization’s capabilities to the existing/new \n"+
//                "customers and help to align the expectations and responsibilities. This \n"+
//                "process is implemented at an organizational level and is revisited as  \n"+
//                "and when there are changes in the organizational strategies. \n")
//    }
//    else if(text1 == "Responsibility/AuthorityOpen Detail") {
//        $('.dia-text').text("Responsibility/Authority \n"+
//                "Senior Management / Delivery Head – to establish the business objectives \n"+
//                "Account Managers – to identify the needs of the customers \n"+
//                "Program Manager – to define the standard set of services \n")
//    }
//    else if(text1 == "Special ConsiderationsOpen Detail") {
//        $('.dia-text').text("Special Considerations \n"+
//                    "Some definitions: \n"+
//                    "Service Catalog: The Service Catalog is the published orderable set of \n"+
//                    "standard services. It contains information on services along with their description. \n")
//    }
//    else if(text1 == "InputsOpen Detail") {
//        $('.dia-text').text("Inputs \n"+
//            "Task order/SOW/Scope document/contract document \n"+
//            "Market research results \n"+
//            "Customer complaints/ Commendations \n"+
//            "Business Plan \n"+
//            "Service Catalog \n"
//            )
//    }
//    else if(text1 == "Entry CriteriaOpen Detail") {
//        $('.dia-text').text("Entry Criteria \n"+
//            "Contract/SOW/Task Order received \n"+
//            "Strategy review frequency met \n"+
//            "Management Review meeting to be held \n"
//            )
//    }
//    else if(text1 == "ActivitiesOpen Detail") {
//        $('.dia-text').text("Activities \n"+
//            "1 \n"+
//            "Establish / Review Service Capabilities  \n"+
//            "1.1 \n"+
//            "The senior management of the company ensures that customer / market \n"+
//            "requirements are collected from various sources like  \n"+
//            "Existing services and the service level agreements \n"+
//            "Customer expectation  \n"+
//            "<ORG> objectives / strategic needs (documented in the business plan) \n"+
//            "Market research \n"+
//            "Customer complaints / Commendations \n"+
//            "Changing customer requirements based on market conditions (e.g. \n" +
//            "more work for lesser cost, value added services)  \n"+
//            "These requirements are analysed to see how well they fit in with the \n" +
//            "business plan of the company. If required, the business plan is reviewed and \n" +
//            "updated to ensure that there is value add to the customers in using our services.  \n"
//            )
//    }
//    else if(text1 == "OutputsOpen Detail") {
//        $('.dia-text').text("Outputs \n"+
//            "Organizational Service Catalog \n"+
//            "Program Service Catalog \n"+
//            "Service Level Agreement  \n"+
//            "Service Planning & Tailoring \n"
//            )
//    }
//    else if(text1 == "Exit CriteriaOpen Detail") {
//        $('.dia-text').text("Exit Criteria \n"+
//            "Release of the service catalog \n"+
//            "Program Planning initiated \n"
//            )
//    }
//    else if(text1 == "TemplatesOpen Detail") {
//        $('.dia-text').text("Templates \n"+
//            "Forms \n"+
//            "Checklists \n"+
//            "Organizational Service Catalog \n"+
//            "Program Service Catalog \n"+
//            "Service Level Agreement Template \n"+
//            "Service Planning & Tailoring Template \n"+
//            "None \n"+
//            "None \n"
//            )
//    }
//    else if(text1 == "Guidelines / ReferencesOpen Detail") {
//        $('.dia-text').text("Guidelines / References \n"+
//            "Standards  \n"+
//            "Tools \n"+
//            "Program Planning Process \n"+
//            "Risk Management Process \n"+
//            "None \n"+
//            "None \n"
//            )
//    }
//    else if(text1 == "Reviews & ApprovalsOpen Detail") {
//        $('.dia-text').text("Reviews & Approvals\n"+
//            "No.\n"+
//            "Document Name\n"+
//            "Reviewed By\n"+
//            "Approved By\n"+
//            "1.\n"+
//            "Organizational Service Catalog\n"+
//            "Delivery Head, SSPG Head\n"+
//            "Delivery Head\n"+
//            "2.\n"+
//            "Program Service Catalog\n"+
//            "Delivery Head\n"+
//            "Delivery Head\n"+
//            "3.\n"+
//            "Service Level Agreement\n"+
//            "\n"+
//            "Delivery Head, Program Manager\n"+
//            "Customer\n"+
//            "Service Planning and Tailoring\n"+
//            "Program Manager, Delivery Head\n"+
//            "Delivery Head\n"
//            )
//    }
//    else if(text1 == "AcronymsOpen Detail") {
//        $('.dia-text').text("Acronyms \n"+
//            "PPB \n"+
//            "Process Performance Baselines \n"+
//            "SLA \n"+
//            "Service Level Agreement \n"+
//            "SOW \n"+
//            "Statement of Work \n"
//            )
//    }
//    else {
//        showUserMessage(text1);
//        sayToBot(text1)
//    }
//});


function sayToBot(text){
    document.getElementById("msg_input").placeholder = "Type your messages here..."
//    document.getElementsByClassName("checkbox_input").value = text

    $.get("chat",
            {
//                csrfmiddlewaretoken:csrf,
                text:text,
            },
            function(jsondata, status){
                if(jsondata["status"]=="success"){
                    response=jsondata["response"];
                    res = response.replace(/^\s*\n/gm, "")
                    showBotMessage(res)
//                    if(response){
//                        response.forEach(function(d) {
//                            for(i in d) {
//                                showBotMessage(d[i]);
//                            }
//                        })
//
//                    }
                }
            });

}

getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
//            $message_input = $('.checkbox_input');
            return $message_input.val();
        };

$("#say").keypress(function(e) {
    if(e.which == 13) {
        $("#saybtn").click();
    }
});

$('.send_message').click(function (e) {
        msg = getMessageText();
        if(msg){
        showUserMessage(msg);
        sayToBot(msg);
    $('.message_input').val('');
//    $('.checkbox_input').val('');
    }
});

$('.message_input').keyup(function (e) {
    if (e.which === 13) {
        msg = getMessageText();
        if(msg){
        showUserMessage(msg);
        sayToBot(msg);
    $('.message_input').val('');
//    $('.checkbox_input').val('');
    }
    }
});

$(document).ready(function() {
    msg_lst = ["Lucie: Hi I am Lucie, <br>your Quality systems expert" , "Please choose a Process", "1-  For Service Continuity", "2- For Service Strategy", "If you want to exit, type Bye!"]
    for(msg in msg_lst) {
        showUserMessage(msg_lst[msg]);
    }
});
