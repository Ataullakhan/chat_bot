import os

from django.http import JsonResponse
from django.shortcuts import render
from chat_bot import settings

import docx
import re
path = settings.MEDIA_ROOT
# file_path = path + '/ReferenceDocs/demo.txt'

##dict of response for each type of intent

file1 = path + "/Comtop_Ref_Processes/Capacity_and_Availability_Management/Capacity_and_Availability_Management_Procedure_V0.1.docx"
Cap_avil_temp = path + "/Comtop_Ref_Processes/Capacity_and_Availability_Management/Template/CAM_Tracking_Matrix_V0.1.xlsx"
IRP_files = path + "/Comtop_Ref_Processes/Incident_Resolution_and_Prevention"

def readtxt(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)


Cap_avil_text = readtxt(file1)
Cap_Avil_intro_text = ''
Cap_Avil_intro_text_Ma = re.finditer(r'(\bPurpose\b)((.*\n){3})', Cap_avil_text)
for client_txt in Cap_Avil_intro_text_Ma:
    Cap_Avil_intro_text = client_txt.group()
    Cap_Avil_intro_text = Cap_Avil_intro_text.lstrip('Purpose')

Cap_Avil_scope_text = ''
Cap_Avil_intro_text_Ma = re.finditer(r'(\bScope\b)((.*\n){12})', Cap_avil_text)
for client_txt in Cap_Avil_intro_text_Ma:
    Cap_Avil_scope_text = client_txt.group()
    Cap_Avil_scope_text = Cap_Avil_scope_text.lstrip('Scope')


IRP_text = readtxt(IRP_files + '/Incident_Resolution_and_Prevention_Ver_1.0.docx')
Irp_intro_text = ''
irp_intro_text_Ma = re.finditer(r'(\bIntroduction\b)((.*\n){5})', IRP_text)
for client_txt in irp_intro_text_Ma:
    Irp_intro_text = client_txt.group()
    Irp_intro_text = Irp_intro_text.lstrip('Introduction')

Irp_Scope_text = ''
irp_intro_text_Ma = re.finditer(r'(\bScope\b)((.*\n){2})', IRP_text)
for client_txt in irp_intro_text_Ma:
    Irp_Scope_text = client_txt.group()
    Irp_Scope_text = Irp_Scope_text.lstrip('Scope')

Irp_lst_pro_text = ''
irp_intro_text_Ma = re.finditer(r'(\bList of Procedures\b)((.*\n){6})', IRP_text)
for client_txt in irp_intro_text_Ma:
    Irp_lst_pro_text = client_txt.group()
    Irp_lst_pro_text = Irp_lst_pro_text.lstrip('List of Procedures')

Irp_sup_pro_text = ''
irp_intro_text_Ma = re.finditer(r'(\bSupport Processes\b)((.*\n){20})', IRP_text)
for client_txt in irp_intro_text_Ma:
    Irp_sup_pro_text = client_txt.group()
    Irp_sup_pro_text = Irp_sup_pro_text.lstrip('Support Processes')


#
IRP_Guidline_text = readtxt(IRP_files + '/IRP_Guideline_Ver 1.0.docx')
Irp_Criteria_text = ''
irp_intro_text_Ma = re.finditer(r'(\bCriteria to identify an Incident\b)((.*\n){7})', IRP_Guidline_text)
for client_txt in irp_intro_text_Ma:
    Irp_Criteria_text = client_txt.group()
    Irp_Criteria_text = Irp_Criteria_text.lstrip('Criteria to identify an Incident')

Irp_categry_text = ''
irp_intro_text_Ma = re.finditer(r'(\bExamples of Categories of Incidents\b)((.*\n){30})', IRP_Guidline_text)
for client_txt in irp_intro_text_Ma:
    Irp_categry_text = client_txt.group()
    Irp_categry_text = Irp_categry_text.lstrip('Examples of Categories of Incidents')

Irp_prioritization_text = ''
irp_intro_text_Ma = re.finditer(r'(\bPrioritization of Incidents\b)((.*\n){11})', IRP_Guidline_text)
for client_txt in irp_intro_text_Ma:
    Irp_prioritization_text = client_txt.group()
    Irp_prioritization_text = Irp_prioritization_text.lstrip('Prioritization of Incidents')



entites_response_dict = {
    "Purpose": [Cap_Avil_intro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Capacity_and_Availability_Management/Capacity_and_Availability_Management_Procedure_V0.1.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Scope": [Cap_Avil_scope_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Capacity_and_Availability_Management/Capacity_and_Availability_Management_Procedure_V0.1.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Template": ["""Templates <br> <a class="file" href='static/Comtop_Ref_Processes/Capacity_and_Availability_Management/Template/CAM_Tracking_Matrix_V0.1.xlsx' download="file.doc"><span class="badge badge-primary">Template Document</span></a>"""],

    "Introduction": [Irp_intro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Scope_for_irp": [Irp_Scope_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "List_of_Procedures_for_irp": [Irp_lst_pro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Support_Processes_for_irp": [Irp_sup_pro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Overview": ["Criteria to identify an Incident <br> Examples of Categories of Incidents <br> Prioritization of Incidents"],
    "Criteria_incident_irp_guied": [Irp_Criteria_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/IRP_Guideline_Ver 1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "irp_guied_Categories_of_Incidents": [Irp_categry_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/IRP_Guideline_Ver 1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Prioritization_irp_guied": [Irp_prioritization_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/IRP_Guideline_Ver 1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Template of INCIDENT RESOLUTION  AND PREVENTION": ["""Templates <br> <a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Report_June_2016-2.xlsx' download="file.doc"><span class="badge badge-primary">Template Document</span></a>"""],


}

intent_response_dict = {
    "cap_avil_intro_text": [Cap_Avil_intro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Capacity_and_Availability_Management/Capacity_and_Availability_Management_Procedure_V0.1.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "cap_avil_scope_text": [Cap_Avil_scope_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Capacity_and_Availability_Management/Capacity_and_Availability_Management_Procedure_V0.1.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Template of Capacity and Availability Management Procedure": ["""Templates <br> <a class="file" href='static/Comtop_Ref_Processes/Capacity_and_Availability_Management/Template/CAM_Tracking_Matrix_V0.1.xlsx' download="file.doc"><span class="badge badge-primary">Template Document</span></a>"""],
    "INCIDENT RESOLUTION  AND PREVENTION": [Irp_intro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Scope of INCIDENT RESOLUTION  AND PREVENTION ": [Irp_Scope_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "List of Procedures for INCIDENT RESOLUTION  AND PREVENTION": [Irp_lst_pro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Support Processes of INCIDENT RESOLUTION  AND PREVENTION": [Irp_sup_pro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Criteria to identify an Incident": [Irp_Criteria_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/IRP_Guideline_Ver 1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Examples of Categories of Incidents": [Irp_categry_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/IRP_Guideline_Ver 1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    "Prioritization of Incidents": [Irp_prioritization_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/IRP_Guideline_Ver 1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],
    " INCIDENT RESOLUTION  AND PREVENTION": ["""Templates <br> <a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Report_June_2016-2.xlsx' download="file.doc"><span class="badge badge-primary">Template Document</span></a>"""],
    "Overview": ["Criteria to identify an Incident <br> Examples of Categories of Incidents <br> Prioritization of Incidents"],
    "Overview of Incident Resolution & Prevention": ["Criteria to identify an Incident <br> Examples of Categories of Incidents <br> Prioritization of Incidents"],
    "Incident Summary": [Irp_intro_text + """<br><br><a class="file" href='static/Comtop_Ref_Processes/Incident_Resolution_and_Prevention/Incident_Resolution_and_Prevention_Ver_1.0.docx' download="file.doc"><span class="badge badge-primary">Refrence Document</span></a>"""],

    # "Root_Cause": ["Incident Summary <br>"
    #                "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                "Open Detail"
    #                "</button>",
    #                "GROUPED INCIDENTS SUMMARY <br>"
    #                "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                "Open Detail"
    #                "</button>",
    #                "RESOLUTION <br>"
    #                 "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                 "Open Detail"
    #                 "</button>",
    #                "ACTIONS TO PREVENT FURTHER OCCURENCE <br>"
    #                 "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                 "Open Detail"
    #                 "</button>"
    #                 ],
    #
    # "SERVICE_CONTINUITY": ["Introduction <br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                        "Service Continuity Process<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                        "Support Processes<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>"
    #                        ],
    #
    # "SERVICE_STRATEGY": ["Purpose<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Responsibility/Authority<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Special Considerations<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                     "Inputs<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Entry Criteria<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Activities<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Outputs<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Exit Criteria<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Templates<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Guidelines / References<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                      "Reviews & Approvals<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>",
    #                    "Acronyms<br>"
    #                         "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                         "Open Detail"
    #                         "</button>"
    #                      ],
    "Who are responsible for Service Strategy Processes": ["Senior Management / Delivery Head – to establish the "
                                                           "business objectives",
                                                           "Account Managers – to identify the needs of the customers",
                                                           "Program Manager – to define the standard set of services"],
    "What is a Service catalogue": ["The Service Catalog is the published orderable"
                                    " set of standard services. It contains information"
                                    " on services along with their description"],
    "What are the entry criteria for a Service Strategy Process": [" Contract/SOW/Task Order received",
                                                                   "Strategy review frequency met",
                                                                   "Management Review meeting to be held"
                                                                   ],
    "What are the inputs for a Service Strategy Process": ["Task order/SOW/Scope document/contract document",
                                                           "Market research results",
                                                           "Customer complaints/ Commendations",
                                                           "Business Plan",
                                                           "Service Catalog"
                                                           ],
    "What are the exit criteria for a Service Strategy Process": ["Release of the service catalog",
                                                                  "Program Planning initiated"
                                                                  ],
    "What are the outputs for a Service Strategy Process": ["Organizational Service Catalog",
                                                            "Program Service Catalog",
                                                            "Service Level Agreement ",
                                                            "Service Planning & Tailoring"],
    "What are the templates for a Service Strategy Process": ["Organizational Service Catalog",
                                                              "Program Service Catalog",
                                                              "Service Level Agreement Template",
                                                              "Service Planning & Tailoring Template"
                                                              ],
    "Who reviews a Organizational Service catalogue": ["Delivery Head", "SSPG Head"],
    "Organizational Service catalogue": ["Delivery Head"],

    "greet": ["Hello"],
    "goodbye": ["Bye"],
    "affirm": ["Cool", "I know you would like it"],
    "thankyou": ["you're welcome."]

    # "Incident Summary": ["Incident Summary <br> <button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'> Open Detail </button>",
    #                      "Management Summary <br>"
    #                      "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                      "Open Detail"
    #                      "</button>",
    #                      "Chronology Of Events <br>"
    #                      "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                      "Open Detail"
    #                      "</button>",
    #                      "Background <br>"
    #                      "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                      "Open Detail"
    #                      "</button>",
    #                      "Symptoms <br>"
    #                      "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>"
    #                      "Open Detail"
    #                      "</button>"
    #                      ],
    # "GROUPED INCIDENTS SUMMARY": [],
    # "RESOLUTION": [],
    # "ACTIONS TO PREVENT FURTHER OCCURENCE": [],
    # "Introduction": [],
    # "Service Continuity Process": [],
    # "Support Processes": [],
    # "Purpose": [],
    # "Responsibility/Authority": [],
    # "Special Considerations": [],
    # "Inputs": [],
    # "Entry Criteria": [],
    # "Activities": [],
    # "Outputs": [],
    # "Exit Criteria": [],
    # "Templates": [],
    # "Guidelines / References": [],
    # "Reviews & Approvals": [],
    # "Acronyms": [],


}
#
# gst_query_value_dict = {
#     "12%":"Non-AC hotels, business class air ticket, frozen meat products, butter, cheese, ghee, dry fruits in packaged form, animal fat, sausage, fruit juices, namkeen and ketchup",
#     "5%":"railways, air travel, branded paneer, frozen vegetables, coffee, tea, spices, kerosene, coal, medicines",
#     "18%":"AC hotels that serve liquor, telecom services, IT services, flavored refined sugar, pasta, cornflakes, pastries and cakes",
#     "28%":"5-star hotels, race club betting,wafers coated with chocolate, pan masala and aerated water",
#     "exempt":"education, milk, butter milk, curd, natural honey, fresh fruits and vegetables, flour, besan"
# }
#
# def gst_info(entities):
#     if entities == None:
#         return "Could not find out specific information about this ..." +  gstinfo_response_dict["faq_link"]
#     if len(entities) == 1:
#         print("alwjnajrognas", gstinfo_response_dict[entities[0]])
#         return gstinfo_response_dict[entities[0]]
#     return "Sorry.." + gstinfo_response_dict["faq_link"]

# def gst_query(entities):
#     if entities == None:
#         return "Could not query this ..." + gstinfo_response_dict["faq_link"]
#     for ent in entities:
#         qtype = ent["type"]
#         qval = ent["entity"]
#         if qtype == "gst-query-value":
#             return gst_query_value_dict[qval]
#
#         return gstinfo_response_dict[entities[0]]
#     return "Sorry.." + gstinfo_response_dict["faq_link"]

