# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import nltk
import numpy as np
import random
import string  # to process standard python strings
import pandas as pd
import re, math

from collections import Counter
from django.http import JsonResponse
# import win32com.client
from django.shortcuts import render
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from chat_bot import settings

media_path = settings.MEDIA_ROOT
WORD = re.compile(r'\w+')
df = pd.read_csv(media_path + '/Chats.csv')
length = df.shape[0]
nltk.download('punkt')  # first-time use only
nltk.download('wordnet')  # first-time use only
GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up", "hey",)
GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]
lemmer = nltk.stem.WordNetLemmatizer()


def hello_world(request):
    """
    :param request:
    :return:
    """
    return render(request, 'home.html',)


def get_cosine(vec1, vec2):
    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])

    sum1 = sum([vec1[x] ** 2 for x in vec1.keys()])
    sum2 = sum([vec2[x] ** 2 for x in vec2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)

    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator


def text_to_vector(text):
    words = WORD.findall(text)
    return Counter(words)


# WordNet is a semantically-oriented dictionary of English included in NLTK.
def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]


remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)


def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))


def greeting(sentence):
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)


def response(user_response, sent_tokens):
    robo_response = ''
    sent_tokens.append(user_response)
    TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
    tfidf = TfidfVec.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx = vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]
    if (req_tfidf == 0):
        robo_response = robo_response + "I am sorry! I don't understand you"
        return robo_response
    else:
        robo_response = robo_response + sent_tokens[idx]
        return robo_response


def chat(request):
    user_response = request.GET.get("text")

    print("Lucie: Hi I am Lucie, your Quality systems expert. Please choose a Process 1- Service Continuity 2- Service Strategy.If you want to exit, type Bye!")
    if user_response == "1":
        path = media_path + '/Service_Continuty.doc'
        request.session['path'] = path
        return JsonResponse({"status": "success", "response": "Ask your Queries Now"})
    elif user_response == "2":
        path = media_path + '/Service_Strategy_Process1.docx'
        request.session['path'] = path
        return JsonResponse({"status": "success", "response": "Ask your Queries Now"})
    else:
        print("in else......")
        session_path = request.session['path']
        f = open(session_path, 'r', errors='ignore')
        raw = f.read()
        raw = raw.lower()
        sent_tokens = nltk.sent_tokenize(raw)  # converts to list of sentences
        word_tokens = nltk.word_tokenize(raw)  # converts to list of words
        flag = True
        resp = ''
        while flag == True:
            user_response = request.GET.get("text")
            user_response = user_response.lower()
            chk = False
            print("user response::: "+ user_response)
            if user_response.lower() != 'bye':
                for i in range(0, length):
                    strng = df.iloc[i, 0] + ""
                    vector1 = text_to_vector(strng.lower())
                    vector2 = text_to_vector(user_response.lower())
                    cosine = get_cosine(vector1, vector2)
                    if cosine > 0.70:
                        resp = "Lucie: " + df.iloc[i, 1]
                        chk = True
                        print(resp)

                if chk == False & (user_response != '1' or user_response != '2'):
                    print("Lucie:::::::: ")
                    print(response(user_response, sent_tokens))
                    sent_tokens.remove(user_response)
                    return JsonResponse({"status": "success", "response": response(user_response, sent_tokens)})
            else:
                flag = False
                print("Lucie: Bye! take care..")
                f.close()
                return JsonResponse({"status": "success", "response": "bye..Take care.."})
            return JsonResponse({"status": "success", "response": resp})

